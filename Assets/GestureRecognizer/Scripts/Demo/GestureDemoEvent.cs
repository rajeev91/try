﻿using UnityEngine;
using System.Collections;
using GestureRecognizer;
using UnityEngine.UI;
using System.Collections.Generic;

// !!!: Drag & drop a GestureRecognizer prefab on to the scene first from Prefabs folder!!!
public class GestureDemoEvent : MonoBehaviour {

    [Tooltip("Messages will show up here")]
    public Text messageArea;
    private List<Sprite> spriteImages = new List<Sprite>();
    public int monsterBlastCnt;
    public int wrongAttemptCnt;
    private Canvas playAgainCanvas;
    public GameObject blastObject;
    public GameObject monsterObject;
    public GameObject shapeObject;
    public GameObject gestureCanvas, resultObject;
    public GameObject jump, up, down, left, right;
    public AudioClip monsterBlast, winningSound;
    // Subscribe your own method to OnRecognition event 
    void OnEnable() {
        GestureBehaviour.OnRecognition += OnGestureRecognition;
        playAgainCanvas = GameObject.FindGameObjectWithTag("canvas_playAgainWin").GetComponent<Canvas>();
        jump = GameObject.FindGameObjectWithTag("btn_jump");
        up = GameObject.FindGameObjectWithTag("btn_up");
        down = GameObject.FindGameObjectWithTag("btn_down");
        left = GameObject.FindGameObjectWithTag("btn_left");
        right = GameObject.FindGameObjectWithTag("btn_right");
        resultObject = GameObject.FindGameObjectWithTag("result");        
        playAgainCanvas.enabled = false;
    }

    // Unsubscribe when this game object or monobehaviour is disabled.
    // If you don't unsubscribe, this will give an error.
    void OnDisable() {
        GestureBehaviour.OnRecognition -= OnGestureRecognition;
    }

    // Unsubscribe when this game object or monobehaviour is destroyed.
    // If you don't unsubscribe, this will give an error.
    void OnDestroy() {
        GestureBehaviour.OnRecognition -= OnGestureRecognition;
    }


    /// <summary>
    /// The method to be called on recognition event
    /// </summary>
    /// <param name="r">Recognition result</param>
    /// 
    /// <remarks>
    /// Implement your own method here. This method will be called by GestureBehaviour
    /// automatically when a gesture is recognized. You can write your own script
    /// in this method (kill enemies, shoot a fireball, or cast some spell etc.)
    /// This method's signature should match the signature of OnRecognition event,
    /// so your method should always have one parametre with the type of Result. Example:
    /// 
    /// void MyMethod(Result gestureResult) {
    ///     kill enemy,
    ///     shoot fireball,
    ///     cast spell etc.
    /// }
    /// 
    /// You can decide what to do depending on the name of the gesture and its score.
    /// For example, let's say, if the player draws the letter of "e" (let's name the 
    /// gesture "Fireball") and it is 50% similar to our "Fireball" gesture, shoot a fireball:
    /// 
    /// void MagicHandler(Result magicGesture) {
    /// 
    ///    if (magicGesture.Name == "Fireball" && magicGesture.Score >= 0.5f) {
    ///        Instantiate(fireball, transform.position, Quaternion.identity);
    ///    }
    /// 
    /// }
    /// 
    /// !: You can name this method whatever you want, but you should use the same name
    /// when subscribing and unsubscribing. If your method's name is MagicHandler like above,
    /// then:
    /// 
    /// void OnEnable() {
    ///   GestureBehaviour.OnRecognition += MagicHandler;
    /// }
    /// </remarks>
    void OnGestureRecognition(Result r) {
        //SetMessage("Gesture is recognized as <color=#ff0000>'" + r.Name + "'</color> with a score of " + r.Score);

        bool shapeSphere = GameObject.FindGameObjectWithTag("shapeA");
        bool shapeCapsule = GameObject.FindGameObjectWithTag("shapeB");
        bool Traingle = GameObject.FindGameObjectWithTag("shapeC");

        spriteImages.Add((Sprite)Resources.Load("green_bar", typeof(Sprite)) as Sprite);
        spriteImages.Add((Sprite)Resources.Load("orange_bar", typeof(Sprite)) as Sprite);
        spriteImages.Add((Sprite)Resources.Load("red_bar", typeof(Sprite)) as Sprite);
        spriteImages.Add(new Sprite());
        if (shapeSphere)
        {
            if(r.Name == "circle" && r.Score > 0.8)
            {
                monsterBlastCnt++;
                blastMonstor();                
                GameObject.FindGameObjectWithTag("gesture_canvas").GetComponent<Canvas>().enabled = false;
                gestureCanvas.SetActive(false);
                Time.timeScale = 1;              
                GameObject.FindGameObjectWithTag("img_monsterProgress").GetComponent<Image>().sprite = spriteImages[monsterBlastCnt];
                if (monsterBlastCnt == 3)
                {
                    gameOver();                   
                }
            }
            else
            {
                wrongAttemptCnt++;
                if(wrongAttemptCnt == 3)
                {
                    wrongAttemptCnt = 0;
                    GameObject.FindGameObjectWithTag("gesture_canvas").GetComponent<Canvas>().enabled = false;
                    gestureCanvas.SetActive(false);
                    Time.timeScale = 1;
                    showButtons();
                }
                else
                {
                    resultObject.SetActive(true);
                    SetMessage("Not the correct one!!! Try Again.");
                }
            }
        }
        else if(shapeCapsule)
        {
            if (r.Name == "rectangle" && r.Score > 0.8)
            {
                monsterBlastCnt++;
                blastMonstor();
                GameObject.FindGameObjectWithTag("gesture_canvas").GetComponent<Canvas>().enabled = false;
                gestureCanvas.SetActive(false);
                Time.timeScale = 1;                
                GameObject.FindGameObjectWithTag("img_monsterProgress").GetComponent<Image>().sprite = spriteImages[monsterBlastCnt];
                if (monsterBlastCnt == 3)
                {
                    gameOver();                   
                }
            }
            else
            {
                wrongAttemptCnt++;
                if (wrongAttemptCnt == 3)
                {
                    wrongAttemptCnt = 0;
                    GameObject.FindGameObjectWithTag("gesture_canvas").GetComponent<Canvas>().enabled = false;
                    gestureCanvas.SetActive(false);
                    Time.timeScale = 1;
                    showButtons();
                }
                else
                {
                    resultObject.SetActive(true);
                    SetMessage("Not the correct one!!! Try Again.");
                }
            }
        }
        else if (Traingle)
        {
            if (r.Name == "triangle" && r.Score > 0.8)
            {
                monsterBlastCnt++;
                blastMonstor();
                GameObject.FindGameObjectWithTag("gesture_canvas").GetComponent<Canvas>().enabled = false;
                gestureCanvas.SetActive(false);
                Time.timeScale = 1;                
                GameObject.FindGameObjectWithTag("img_monsterProgress").GetComponent<Image>().sprite = spriteImages[monsterBlastCnt];
                if (monsterBlastCnt == 3)
                {
                    gameOver();                   
                }
            }
            else
            {
                wrongAttemptCnt++;
                if (wrongAttemptCnt == 3)
                {
                    wrongAttemptCnt = 0;
                    GameObject.FindGameObjectWithTag("gesture_canvas").GetComponent<Canvas>().enabled = false;
                    gestureCanvas.SetActive(false);
                    Time.timeScale = 1;
                    showButtons();
                }
                else
                {
                    resultObject.SetActive(true);
                    SetMessage("Not the correct one!!! Try Again.");
                }
            }
        }
    }
    public void GestureReset()
    {
        wrongAttemptCnt = 0;
        monsterBlastCnt = 0;
    }
     void blastMonstor()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = monsterBlast;
        audio.Play();
        blastObject.SetActive(true);
        monsterObject.SetActive(false);
        shapeObject.SetActive(false);
        if (monsterBlastCnt != 3)
            StartCoroutine(DoAnimation());
    }
    void gameOver()
    {
        StartCoroutine(endGame());       
    }
    void showButtons()
    {
        jump.SetActive(true);
        up.SetActive(true);
        down.SetActive(true);
        left.SetActive(true);
        right.SetActive(true);
    }
    IEnumerator HideMessage()
    {
        yield return new WaitForSeconds(2f); // wait for two seconds.
       
    }
    IEnumerator DoAnimation()
    {            
        yield return new WaitForSeconds(2f); // wait for two seconds.
        blastObject.SetActive(false);
        monsterObject.SetActive(true);
        shapeObject.SetActive(true);
        showButtons();
    }
    IEnumerator endGame()
    {
        yield return new WaitForSeconds(2f);
        blastObject.SetActive(false);
        monsterObject.SetActive(true);
        shapeObject.SetActive(true);
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = winningSound;
        audio.Play();
        GameObject.FindGameObjectWithTag("img_monsterProgress").GetComponent<Image>().enabled = false;
        monsterBlastCnt = 0;
        playAgainCanvas = GameObject.FindGameObjectWithTag("canvas_playAgainWin").GetComponent<Canvas>();
        playAgainCanvas.enabled = true;
        Time.timeScale = 0;
    }
    /// <summary>
    /// Shows a message at the bottom of the screen
    /// </summary>
    /// <param name="text">Text to show</param>
    public void SetMessage(string text) {
        messageArea.text = text;
    }
}
